import { HappyTestingPage } from './app.po';

describe('happy-testing App', function() {
  let page: HappyTestingPage;

  beforeEach(() => {
    page = new HappyTestingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
